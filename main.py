#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages
# This program is dedicated to the public domain under the CC0 license.
"""
This Bot uses the Updater class to handle the bot.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

from telegram import (ReplyKeyboardMarkup)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
ConversationHandler)
import logging, requests

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

avail=[[]]
kode=[]    
TANGGAL="1"
REMARK="2"
IDPEMESAN="3"
QUANTITY = "4"
date=0
qty=0
name = 0
remarks=""
idp=[]
nmp=[]

# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    update.message.reply_text('Hi!')


def help(bot, update):
    update.message.reply_text('Help!')

def showmenu(bot, update):
    import urllib2, json
    from bs4 import BeautifulSoup
    req = urllib2.Request('http://kedai75.com/OrderandBalance/order.php?idm=1')
    req.add_header('user-agent', 'Web-sniffer/1.1.0 (+http://web-sniffer.net/)')
    resp = urllib2.urlopen(req)
    soup = BeautifulSoup(resp.read(), 'html.parser')
    #print(soup)
    global idp
    global nmp
    cust=soup.find_all('option')
    for i in cust:
        nmp.append(i.string)
        idp.append(i['value'])



    soup = BeautifulSoup(resp.read(), 'html.parser')

    req = urllib2.Request('http://kedai75.com/OrderandBalance/dailymenu.php')
    req.add_header('user-agent', 'Web-sniffer/1.1.0 (+http://web-sniffer.net/)')
    resp = urllib2.urlopen(req)

    soup = BeautifulSoup(resp.read(), 'html.parser')
    stri=""
    boole=[]
    avail[0]=[]
    global kode
    kode =[]
    for i in range(32):
        boole.append(0)
    for link in soup.find_all('td'):
        keyw=link.string
        if not link.string is None and link.string.isdigit():
            boole[int(link.string)]=True
            #print "%s nilainya %r" % (link.string,boole[int(link.string)])
    for link in soup.find_all('p'):
        if(link.string!='Place an Order'):
            for i in range(31):
                if(not boole[i+1]):
                    boole[i+1]=True
                    stri+=str(i+1)+" September 2016: "+link.string+"\n"
                    break
        else:
            stri+="MASIH ADA\n"
            avail[0].append(str(i+1)+" September 2016")
            if not link.a is None:
                kode.append(link.a['href'].split("=")[1])
            #kode[0].append
    req = urllib2.Request('http://kedai75.com/OrderandBalance/dailymenu.php?month=10&year=2016')
    req.add_header('user-agent', 'Web-sniffer/1.1.0 (+http://web-sniffer.net/)')
    resp = urllib2.urlopen(req)

    soup = BeautifulSoup(resp.read(), 'html.parser')
    boole2=[]
    for i in range(32):
        boole2.append(0)
    for link in soup.find_all('td'):
        keyw=link.string
        if not link.string is None and link.string.isdigit():
            boole2[int(link.string)]=True
            #print "%s nilainya %r" % (link.string,boole[int(link.string)])
    for link in soup.find_all('p'):
        if(link.string!='Place an Order'):
            for i in range(31):
                if(not boole2[i+1]):
                    boole2[i+1]=True
                    stri+=str(i+1)+" Oktober 2016: "+link.string+"\n"
                    break
        else:
            stri+="MASIH ADA\n"
            avail[0].append(str(i+1)+" Oktober 2016")
            if not link.a is None:
                kode.append(link.a['href'].split("=")[1])
    #update.message.reply_text(stri,reply_markup=ReplyKeyboardMarkup(avail, one_time_keyboard=True))
    update.message.reply_text(stri)

def order(bot, update):
    showmenu(bot,update)
    if(len(avail[0])==0):
        update.message.reply_text("Ga ada slot yang kosong...")
    else:
        strtemp="Pesen yang mana?\nKetik /cancel untuk membatalkan pesanan\n"
        arrdum=[[]]
        for i in range(0,len(avail[0])):
            strtemp+=str(i+1)+" : "+avail[0][i]+"\n"
            arrdum[0].append(str(i+1))
        #update.message.reply_text(strtemp,reply_markup=ReplyKeyboardMarkup(avail, one_time_keyboard=True))
        update.message.reply_text(strtemp,reply_markup=ReplyKeyboardMarkup(arrdum[0], one_time_keyboard=True))
    return TANGGAL       

def orderdate(bot, update):
    if(update.message.text.isdigit()):
        #update.message.reply_text(kode[int(update.message.text)-1])
        print kode[int(update.message.text)-1]
    global date    
    date=kode[int(update.message.text)-1]
    user = update.message.from_user
    update.message.reply_text("Berapa banyak?\nKetik /cancel untuk membatalkan pesanan\n")
    return QUANTITY

def orderquantity(bot, update):
    #if(update.message.text.isdigit()):
        #update.message.reply_text(kode[int(update.message.text)-1])
    #    print kode[int(update.message.text)-1]
    global qty
    qty=int(update.message.text)
    user = update.message.from_user
    strt="Masukkan nomor ID Anda sesuai nomor di bawah!\n"
    for i in range(0,len(idp)):
        strt+=idp[i]+" : "+nmp[i]+"\n"
    update.message.reply_text(strt+"Ketik /cancel untuk membatalkan pesanan\n")
    return IDPEMESAN

def ordername(bot, update):
    global name
    name=int(update.message.text)
    user = update.message.from_user
    update.message.reply_text("Remark?\nKetik /cancel untuk membatalkan pesanan\n")
    return REMARK

def orderremark(bot, update):
    #if(update.message.text.isdigit()):
        #update.message.reply_text(kode[int(update.message.text)-1])
    #    print kode[int(update.message.text)-1]
    global remark
    remark=update.message.text
    update.message.reply_text("Order Sent!\nPlease make sure that your order is confirmed on http://kedai75.com/OrderandBalance/allorder.php?idm="+str(date))
    print(str(date)+" "+str(qty)+" "+str(name)+" "+remark)
    r=requests.post("http://kedai75.com/OrderandBalance/order.php", data={'id_menu': date, 'qty': qty, 'name': name,'remark':remark},headers={'user-agent': 'Web-sniffer/1.1.0 (+http://web-sniffer.net/)'})
    print(r.text)
    return ConversationHandler.END




def cancel(bot, update):
    update.message.reply_text("Order canceled!")
    return ConversationHandler.END    

def echo(bot, update):
    update.message.reply_text(update.message.text)


def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))


def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater("246766379:AAEoCqUKy1jC0UcQ3iifHVKJFYn6W_uiy5s")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("showmenu",showmenu))
    
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('order', order)],
        states={
            TANGGAL: [MessageHandler([Filters.text], orderdate),CommandHandler('cancel', cancel)],
            QUANTITY: [MessageHandler([Filters.text], orderquantity),CommandHandler('cancel', cancel)],
            IDPEMESAN: [MessageHandler([Filters.text], ordername),CommandHandler('cancel', cancel)],
            REMARK: [MessageHandler([Filters.text], orderremark),CommandHandler('cancel', cancel)],
 #                   CommandHandler('skip', skip_photo)],

#            LOCATION: [MessageHandler([Filters.location], location),
 #                      CommandHandler('skip', skip_location)],

  #          BIO: [MessageHandler([Filters.text], bio)]
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)
    
    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler([Filters.text], echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
